from models.cardio_tissue import CardioTissue
import numpy as np
import matplotlib.pyplot as plt


class APmu(CardioTissue):
    def __init__(self):
        CardioTissue.__init__(self)
        
        self._initialize_model_variables()
        
        self._v = np.array([])  # current variable
        
    def _initialize_model_variables(self):
        self._a = 0.1
        self._k = 8.
        self._eap = 0.01
        self._mu_1 = 0.2
        self._mu_2 = 0.3
        self._el_ms = 10
        self._D = 0.2
        
    def _initialize_mesh(self):
        self._u = np.zeros([self._n, self._n])
        self._v = np.zeros([self._n, self._n])
        
    def _compute_model_units(self):
        self._D = self._D*self._el_ms
        self._t_max = self._t_max/self._el_ms
        self._dt = self._dt/self._el_ms
        self._s2_time = self._s2_time/self._el_ms
        self._lvd_time_start = self._lvd_time_start/self._el_ms
        self._lvd_time_end = self._lvd_time_end/self._el_ms
        self._lvd_period = self._lvd_period/self._el_ms
        
    def set_model_parameters(self, model_dict):
        self._a = model_dict["a"]
        self._k = model_dict["k"]
        self._eap = model_dict["eap"]
        self._mu_1 = model_dict["mu1"]
        self._mu_2 = model_dict["mu2"]
        self._el_ms = model_dict["el_ms"]
        self._D = model_dict["D"]
        
    def compute(self):
        """Calculates u(t) and v(t) in every moment of time from 0 to t_max 
        with dt step. Displays time.
        """
        
        # initialization:
        self._compute_model_units()
        self._initialize_mesh()
        
        # s1 stimul (or just initial stimul)
        self.set_rectang_stim(self._s1_stim, self._stim_value)
        
        # check if wave_detector was connected:
        if self._detect_mode:
            self._wave_detector.set_n(self._n)
            self._wave_detector.set_dr(self._dr)
            

        t = 0.
        lvd_t = 0.       # to track the lvd period
        wave_det_t = 0.  # to track the wave detector
        while t < self._t_max:
            # s2 stimul to initiate spiral wave:
            if t >= self._s2_time and self._s2_mode:
                self.set_rectang_stim(self._s2_stim, self._stim_value)
                self._s2_mode = False
            
            # lvd stimul with certain period:
            if lvd_t >= self._lvd_period and \
                self._lvd_time_end > t >= self._lvd_time_start:
                    self.set_rectang_stim(self._lvd_stim, self._lvd_stim_value)
                    lvd_t = 0.

            # calculations:
            self._u = self._dt*self.__get_laplace() + self._dt*self.__get_source() + self._u
            self.__euler()
            
            # spiral wave detection (write tips to file) 
            # if was connected:
            if wave_det_t >= self._detect_period and self._detect_mode:
                self._wave_detector.write_tips(self._u, t)
                wave_det_t = 0
            
            # time increment:
            t += self._dt
            lvd_t += self._dt   
            wave_det_t += self._dt
            
        plt.imshow(self._u)
        plt.colorbar()
        plt.show()

        
        # file (tips) closing:
        if self._detect_mode:
            self._wave_detector.close_file()
            
    def get_model_type(self):
        return "Aliev-Panfilov"


    # NUMERICAL SOLUTION METHODS
    # Five-points stencil for laplacian calculation
    # Euler method for current ode 
    # Implemented as private methods to calls in compute() method
        
    def __get_laplace(self):
        """Returns laplassian of the action potential function. 
    Two-dimentional; takes no arguments.
    """
        du_dy_2 = (np.roll(self._u, -1, axis=0) - 2*self._u + np.roll(self._u, 1, axis=0))/(self._dr**2)
        du_dx_2 = (np.roll(self._u, -1, axis=1) - 2*self._u + np.roll(self._u, 1, axis=1))/(self._dr**2)
        laplace = self._D*(du_dx_2 + du_dy_2)
        
        # boundary conditions:
        laplace[0, :] = laplace[1, :]
        laplace[-1, :] = laplace[-2, :]
        laplace[:, 0] = laplace[:, 1]
        laplace[:, -1] = laplace[:, -2]
        
        return laplace

    
    def __get_source(self):
        """Calculate internal source
    """
        return -self._k*self._u*(self._u-self._a)*(self._u-1.)-self._u*self._v
        
    def __euler(self):
        """Returns fast function v by Euler method. 
    Takes no arguments.
    """
        self._v += -self._dt*(self._eap + (self._mu_1*self._v)/(self._mu_2 + self._u))*(self._v + self._k*self._u*(self._u - self._a - 1.))
