from abc import ABCMeta, abstractmethod
import numpy as np


class CardioTissue:
    # Abstract class which provides a basic interface
    # to all electrophisiological models on 2D domain
    #
    # override those methods in inherited classes:
    # compute()
    # set_model_parameters()
    # get_model_type()
    
    __metaclass__ = ABCMeta
    
    def __init__(self):
        self._initialize_calc_parameters()
        self._initialize_stim_parameters()
        self._initialize_lvd_parameters()
        
        self._prior = 0
        self._u = np.array([])  # transmembran potential
        
        self._wave_detector = None
        
    def _initialize_calc_parameters(self):
        self._n = 1
        self._t_max = 0.
        self._dr = 0.1
        self._dt = 0.01
        
    def _initialize_stim_parameters(self):
        self._s1_stim = []
        self._s2_mode = False
        self._s2_stim = []
        self._s2_time = 0.
        self._stim_value = 1.
        
    def _initialize_lvd_parameters(self):
        self._lvd_mode = False
        self._lvd_stim = []
        self._lvd_period = 0.
        self._lvd_time_start = 0.
        self._lvd_time_end = 0.
        self._lvd_stim_value = 1.
        self._detect_mode = False
        self._detect_period = 0.
        
    def connect_with_wave_detector(self, wave_detector):
        self._wave_detector = wave_detector
        
    def set_file_tips(self, file_name):
        self._wave_detector.set_file(file_name)
        
    @abstractmethod
    def compute(self):
        ...
    
    @abstractmethod
    def set_model_parameters(self, model_dict):
        ...
        
    def set_priority(self, k):
        self._prior=k
   
    def set_rectang_stim(self, stim_coords, stim_value):
        self._u[stim_coords[0]:stim_coords[1], stim_coords[2]:stim_coords[3]] = stim_value
    
    def set_calc_parameters(self, calc_dict):
        self._n = calc_dict["side_nodes"]
        self._t_max = calc_dict["t_max"]
        self._dr = calc_dict["dr"]
        self._dt = calc_dict["dt"]
        
    def set_stim_parameters(self, stim_dict):
        if stim_dict["stim_area"] == "rectangular":
            self._s1_stim = stim_dict["s1_coordinates"]
            self._stim_value = stim_dict["stim_value"]
        elif stim_dict["stim_area"] == "s1_s2":
            self._s1_stim = stim_dict["s1_coordinates"]
            self._stim_value = stim_dict["stim_value"]
            self._s2_time = stim_dict["s2_time"]
            self._s2_stim = stim_dict["s2_coordinates"]
            self._s2_mode = True
            
    def set_lvd_parameters(self, lvd_dict):
        self._lvd_mode = lvd_dict["LVD_mode"]
        self._lvd_stim = lvd_dict["coordinates"]
        self._lvd_period = lvd_dict["period"]
        self._lvd_time_start = lvd_dict["time_start"]
        self._lvd_time_end = lvd_dict["time_end"]
        self._lvd_stim_value = lvd_dict["stim_value"]
        self._detect_mode = lvd_dict["detect_mode"]
        self._detect_period = lvd_dict["detect_period"]

    def get_priority(self):
        return self._prior
    
    def get_lvd_mode(self):
        return self._lvd_mode
    
    @abstractmethod
    def get_model_type(self):
        # should return string value with the model name 
        ...

    
    
