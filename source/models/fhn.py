from models.cardio_tissue import CardioTissue
import numpy as np

import matplotlib.pyplot as plt

class FHN(CardioTissue):
    def __init__(self):
        CardioTissue.__init__(self)
        
        self._initialize_model_variables()
        
        self._v = np.array([])  # current variable
        
    def _initialize_model_variables(self):
        self._a = -0.1
        self._e = 0.005
        self._gamma = 0.001
        self._D = 0.1
        
    def _initialize_mesh(self):
        self._u = np.zeros([self._n, self._n])
        self._v = np.zeros([self._n, self._n])
        
    def set_model_parameters(self, model_dict):
        self._a = model_dict["a"]
        self._e = model_dict["e"]
        self._gamma = model_dict["gamma"]
        self._D = model_dict["D"]
        
    def compute(self):
        """Calculates u(t) and v(t) in every moment of time from 0 to t_max 
        with dt step. Displays time.
        """
        self._initialize_mesh()
        self.set_rectang_stim(self._s1_stim, self._stim_value)
        
        ls_u = []
        ls_t = []

        t = 0.
        lvd_t = 0. # to track the lvd period
        while t < self._t_max:
            self._u = self._dt*self.__get_laplace() + self._dt*self.__get_source() + self._u
            self.__euler()
            t += self._dt
            lvd_t += self._dt
            ls_t.append(t)
            ls_u.append(self._u[50,50])
            
        plt.plot(ls_t, ls_u)
        plt.show()
        
    def get_model_type(self):
        return "FHN"


    # NUMERICAL METHODS SECTION
    # Five-points stencil for laplacian calculation
    # Euler method for current ode 
    # Implemented as private methods to calls in compute() method
        
    def __get_laplace(self):
        """Returns laplassian of the action potential function. 
    Two-dimentional; takes no arguments.
    """
        du_dy_2 = (np.roll(self._u, -1, axis=0) - 2*self._u + np.roll(self._u, 1, axis=0))/(self._dr**2)
        du_dx_2 = (np.roll(self._u, -1, axis=1) - 2*self._u + np.roll(self._u, 1, axis=1))/(self._dr**2)
        laplace = self._D*(du_dx_2 + du_dy_2)
        
        # boundary conditions:
        laplace[0, :] = laplace[1, :]
        laplace[-1, :] = laplace[-2, :]
        laplace[:, 0] = laplace[:, 1]
        laplace[:, -1] = laplace[:, -2]
        
        return laplace
    
    def __get_source(self):
        """Calculate internal source
    """
        return (self._u - self._a)*(1 -self._u)*self._u - self._v
        
    def __euler(self):
        """Returns fast function v by Euler method. 
    Takes no arguments.
    """
        self._v += self._e*(self._u - self._gamma*self._v)

if __name__ == "__main__":
    
    calc_param_dict = {"side_nodes": 150,
                       "t_max": 1000,
                       "dr": 0.5,
                       "dt": 0.05}
    stim_parameters = {"stim_area": "rectangular",
                       "s1_coordinates": [0, 5, 0, 150],
                       "stim_value": 1.}
    
    fhn = FHN()
    fhn.set_calc_parameters(calc_param_dict)
    fhn.set_stim_parameters(stim_parameters)
    fhn.compute()
    
    

