from math import sqrt
import numpy as np
import warnings


class WaveDetector:
    def __init__(self):
        self._n = 100
        self._dr = 0.5 
        
        self._u_next_step = np.array([])
        self._u_curr_step = np.array([])
    
    def set_n(self, n):
        self._n = n
        self._u_next_step = np.zeros([n, n], dtype="Float64")
        self._u_curr_step = np.zeros([n, n], dtype="Float64")
        
    def set_dr(self, dr):
        self._dr = dr
        
    def set_file(self, file_name):
        self._file_name = file_name
        
    def close_file(self):
        self._file.close()
    
    def calc_tippos(self, vij, vi1j, vi1j1, vij1, vnewij, vnewi1j,
                    vnewi1j1, vnewij1, V_iso1, V_iso2, xy):
        # For old voltage values of point and neighbours
        AC= vij - vij1 + vi1j1 - vi1j  # upleft-downleft + downright-upright
        GC= vij1 - vij                 # downleft-upleft
        BC= vi1j - vij                 # upright-upleft
        DC= vij - V_iso1               # upleft-iso

        # For current voltage values of point and neighbours
        AD= vnewij - vnewij1 + vnewi1j1 - vnewi1j
        GD= vnewij1 - vnewij
        BD= vnewi1j - vnewij
        DD= vnewij - V_iso2  # adapted here
        
        # Ignore float arithmetic warnings:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            Q= BC*AD - BD*AC
            R= GC*AD - GD*AC
            S= DC*AD - DD*AC

            QOnR=Q/R
            SOnR=S/R

            T = AC*QOnR
            U = AC*SOnR - BC + GC*QOnR
            V = GC*SOnR - DC

            # Compute discriminant of the abc formula
            # with a=T, b=U and c=V
            Disc = U*U-4.*T*V
            if Disc<0:
                # If the discriminant is smaller than
                # zero there is no solution and a
                # failure flag should be returned
                return 0
            else:
            # Otherwise two solutions for xvalues
                T2 = 2.*T
                sqrtDisc = sqrt(Disc)

                xn = (-U-sqrtDisc)/T2
                xp = (-U+sqrtDisc)/T2
                # Leading to two solutions for yvalues
                yn = -QOnR*xn - SOnR
                yp = -QOnR*xp - SOnR

            # demand that fractions lie in interval [0,1]
                if(xn >= 0 and xn <= 1 and yn >= 0 and yn <= 1):
                    # If the first point fulfills these
                    # conditions take that point
                    xy[0] = xn
                    xy[1] = yn
                    return 0
                elif (xp >= 0 and xp <= 1 and yp >= 0 and yp <= 1):
                    # If the second point fulfills these
                    # conditions take that point
                    xy[0] = xp
                    xy[1] = yp
                    return 1
                else:
                    # If neither point fulfills these
                    # conditions return a failure flag.
                    return 0
    
    def track_tipline(self, var1, var2, tipvals, tipdata, tipsfound):
        iso1 = tipvals[0]
        iso2 = tipvals[1]
        
        # each row contains data for a point of the tipline
        # stored in columns: type, posx posy posz, dxu dyu dzu, dxv dyv dzv, tx ty tx
        # possible types: 1 = YZ, 11 = YZ at lower medium boundary, 21 = YZ at upper medium boundary,
        # 31 at lower domain boundary, 41 at upper domain boundary
        # 2 = XZ, 12 = XZ at lower medium boundary, 22 = XZ at upper medium boundary
        # 3 = XY, 13 = XY at lower medium boundary, 23 = XY at upper medium boundary
        # sign of type equals < T , e_i > (i.e. entering or leaving the voxel)

        tipsfound[0] = 0
        delta = 10

        size = [self._n, self._n]

        # check XY-planes
        for xpos in range(delta, size[0] - delta):
            for ypos in range(delta, size[1]-delta):
                if tipsfound[0] >= 100:
                    self._warning_label()
                counter = 0
                if var1[xpos][ypos] >= iso1 and \
                    var1[xpos+1][ypos] < iso1 or \
                    var1[xpos][ypos+1] < iso1 or \
                    var1[xpos+1][ypos+1] < iso1:
                    counter = 1
                else:
                    if var1[xpos][ypos] < iso1 and \
                        var1[xpos+1][ypos] >= iso1 or \
                        var1[xpos][ypos+1] >= iso1 or \
                        var1[xpos+1][ypos+1] >= iso1:
                        counter = 1
                if counter == 1:
                    if var2[xpos][ypos] >= iso2 and \
                        var2[xpos+1][ypos] < iso2 or \
                        var2[xpos][ypos+1] < iso2 or \
                        var2[xpos+1][ypos+1] < iso2:
                        counter = 2
                    else:
                        if var2[xpos][ypos] < iso2 and \
                            var2[xpos+1][ypos] >= iso2 or \
                            var2[xpos][ypos+1] >= iso2 or \
                            var2[xpos+1][ypos+1] >= iso2:
                            counter = 2

                    if counter == 2:
                        tipfco = [0, 0]
                        if self.calc_tippos(var1[xpos][ypos], var1[xpos+1][ypos], var1[xpos+1][ypos+1], var1[xpos][ypos+1], 
                            var2[xpos][ypos], var2[xpos+1][ypos], var2[xpos+1][ypos+1], var2[xpos][ypos+1], iso1, iso2, tipfco):
                            tipdata[tipsfound[0]][0] = xpos + tipfco[0]
                            tipdata[tipsfound[0]][1] = ypos + tipfco[1]

                            tipsfound[0] += 1
    
    def write_tips(self, u, t):
        self._u_next_step = np.copy(u)
        
        self._file = open(self._file_name, "w")
        
        tipdata = np.zeros([102, 2])

        tipvals = [0, 0]
        tipvals[0] = 0.5
        tipvals[1] = 0.5

        tipsfound = [0,]

        self.track_tipline(self._u_curr_step, self._u_next_step, tipvals, tipdata, tipsfound);

        # print ("Found %i tip(s)\n",tipsfound);
        if tipsfound[0] > 0:
            for i in range(tipsfound[0]):
                self._file.write(str(int(t)) + " " + str(i) + " ")
                for j in range(2):
                    self._file.write(str(tipdata[i][j]*self._dr) + " ")
                self._file.write("\n")

        self._u_curr_step = np.copy(self._u_next_step)

    def _warning_label(self):
        if tipsfound > 100-2:
            print ("Warning from tracktipline.m: maximum number of tips tracked reached")
            tipsfound_ = tipsfound 
