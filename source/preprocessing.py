import json


class Preprocessing():
    def __init__(self):
        self._json_struct = None
        
    def open_json(self, fname):
        file_ = open(fname)
        self._json_struct = json.load(file_)
        file_.close()

    def get_sections_number(self):
        """Returns the sections amount"""
        return len(self._json_struct)
    
    def get_experiments_number(self):
        """Returns the experiments amount"""
        counter = 0
        for i in range(len(self._json_struct)):
            counter += len(self._json_struct[i]['experiments_stack'])
        return counter
    
    def get_experiment_priority(self, n):
        """Returns the experiment's priority;
    demantds experiment's number
    """
        m, k = self._calculate_exact_numbers(n)
        return self._json_struct[m]['experiments_stack'][k]['priority']
    
    def get_model_parameters(self, n):
        """Returns experiment model's properties;
    demantds experiment's number
    """
        m = self._calculate_exact_numbers(n)[0]
        return self._json_struct[m]['model']
    
    def get_calculation_parameters(self, n):
        """Returns experiment calculation's parameters;
    demantds experiment's number
    """
        m = self._calculate_exact_numbers(n)[0]
        return self._json_struct[m]['calculation']
    
    def get_init_conditions(self, n):
        """Returns experiment initial conditions' parameters;
    demantds experiment's number
    """
        m = self._calculate_exact_numbers(n)[0]
        return self._json_struct[m]['initial conditions']
    
    def get_lvd_conditions(self, n):
        
        m, k = self._calculate_exact_numbers(n)
        return self._json_struct[m]['experiments_stack'][k]['e1'] 
    
    def _calculate_exact_numbers(self, n):
        counter = 0
        for i in range(len(self._json_struct)):
            for j in range(len(self._json_struct[i]['experiments_stack'])):
                if counter == n:
                    return i, j
                counter += 1
