import multiprocessing as mp
from preprocessing import Preprocessing
from models.apmu import APmu
from wave_detector import WaveDetector
import sys
import threading
import time
import tabulate


class ProcessManager:
    """
    Commands:
    list     - get the processes number
    run      - start all processes
    stop <n> - terminate process n
    exit     - exit program 
    """

    def __init__(self, preprocessing):
        self._preproc = preprocessing
        self._num = self._preproc.get_experiments_number()
        self._task_list = []
        self._launched_list = []
        self._jobs = []
        # function to check process state: A if active or I if inactive:
        self._check_status_func = lambda status: "A" if status else "I"
        self._available_units = 3  # or mp.cpu_count()

    def queue(self):
        priority_dict = {}
        for i in range(self._num):
            priority_dict[i] = self._preproc.get_experiment_priority(i)
        
        self._task_list=[]
        for i in sorted(priority_dict, key=priority_dict.get):
            self._task_list.append(APmu())
            self._task_list[-1].set_priority(priority_dict[i])
            self._task_list[-1].set_model_parameters(self._preproc.get_model_parameters(i))
            self._task_list[-1].set_calc_parameters(self._preproc.get_calculation_parameters(i))
            self._task_list[-1].set_stim_parameters(self._preproc.get_init_conditions(i))
            
            # LVD initialization:
            self._task_list[-1].set_lvd_parameters(self._preproc.get_lvd_conditions(i))
                        
            # work detector initialization:
            if self._task_list[-1].get_lvd_mode():
                wave_detector = WaveDetector()
                wave_detector.set_file("detection_" + str(i) + ".txt")
                self._task_list[-1].connect_with_wave_detector(wave_detector)
            
        
    def _exit(self):
        ex=input('Are you sure you want to exit the program? Print y/n\n').lower()
        if ex=='y':
            sys.exit()

    def _stop(self, n):
        if self._jobs[n].is_alive():
            self._jobs[n].terminate()
        else:
            print('Process is not active')
    
    def _run(self):
        for i in range(len(self._jobs)):
            if i == self._available_units:
                break
            self._jobs[i].start()
            self._launched_list[i] = True
            
        jobs_tracker = threading.Timer(1, self._update_jobs)
        jobs_tracker.start()
            
    def _process_list(self):
        table = []
        for i in range(len(self._jobs)):
            table.append([i, 
                          self._task_list[i].get_priority(), 
                          self._task_list[i].get_model_type(),
                          self._check_status_func(self._jobs[i].is_alive())])

        print (tabulate.tabulate(table, headers=["Number", "Priority", "Model", "State"]))

    def execute(self):
        self._jobs=[]
        for task in self._task_list:
            self._jobs.append(mp.Process(target=task.compute))
            self._launched_list.append(False)
               
        self._processing_loop()
            
    def _processing_loop(self):       
        while True:
            user_input = input("Enter the comand: ").lower()

            if user_input == "run":
                self._run()
            elif user_input == "list":
                self._process_list()
            elif "stop" in user_input:
                try:
                    n = int(user_input.split("stop")[1])
                    if n <= self._num-1:
                        self._stop(n)
                    else:
                        print ("Process does not exist")
                except Exception:
                    print ("Cannot parse command")
                    print ("Format: stop <number>")
            elif user_input == "exit":
                self._exit()
            else:
                print ("'" + user_input + "' command not found")
                
    def _update_jobs(self):       
        # check if jobs already finished:
        while True:
            index_list = []
            for i in range(len(self._jobs)):
                if not self._jobs[i].is_alive() and self._launched_list[i]:
                    index_list.append(i)
            if index_list:
                for index in sorted(index_list)[::-1]:
                    del self._jobs[index]
                    del self._task_list[index]
                    del self._launched_list[index]
            
            if not self._jobs:
                return 
            workers = 0
            for job in self._jobs:
                if job.is_alive():
                    workers += 1
            if workers < self._available_units:
                for i in range(self._available_units - workers):
                    if i >= len(self._jobs):
                        continue  # a dirty way to prevent IndexError 
                    if not self._jobs[i].is_alive() and not self._launched_list[i]:
                        self._jobs[i].start()
                        self._launched_list[i] = True
            time.sleep(1)
                    
            
if __name__ == '__main__':
    import os
    
    preproc = Preprocessing()
    preproc.open_json(os.path.join("..", "examples", "lvd_three_experiments.json"))
    proc_manag = ProcessManager(preproc)
    proc_manag.queue()
    proc_manag.execute()
        
